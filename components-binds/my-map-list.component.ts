import {Component, EventEmitter, OnInit, Output} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {IMap} from "../../../models/map";
import {MapService} from "../../../shared/services/map.service";

@Component({
  selector: 'my-map-list',
  templateUrl: './my-map-list.component.html',
  styleUrls: [ './my-map-list.component.sass' ]
})
export class MyMapList implements OnInit {
  @Output() changeMap: EventEmitter<any> = new EventEmitter<any>();
  maps$: Observable<IMap[]>;
  maps: IMap[] = [];
  myStr = '    ';
  toggle: boolean = false;

  constructor(private _mapService: MapService){
    this.maps$ = this._mapService.getMapsList();
  }

  ngOnInit() {
    this._mapService.initMapsList();
    this.maps$
      .subscribe( (maps: IMap[]) => {
          this.maps = maps;
          console.log("here",maps);
        }
      )
  }

  found(){
    this.toggle=!this.toggle;
    this.maps$ = this._mapService.getMapsList();
  }

  callToChangeMap(mapHandle: string){
    this.changeMap.next(mapHandle);
  }

}